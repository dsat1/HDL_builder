#!/usr/bin/env python3

import os
import sys
# caution: path[0] is reserved for script path (or '' in REPL)

sys.path.append(os.path.join(os.path.dirname(__file__),'external/dsat/lib'))
#sys.path.insert(1, 'external/dsat/lib')


from copy import copy
try:
    from libdsat import *
except ModuleNotFoundError:
    print("libdsat module not found. Are the Python bindings installed?")
    sys.exit(1) 

import pprint
import string

from jinja2 import Template

#################################################################################
# Language stuctures
#################################################################################
class PackageStruct:
    def __init__(self,
                 baseName            = "Ctrl",
                 fmt_complexType     = "{0: <30} : {1}{2}_t;",
                 fmt_bitType         = "{0: <30} : std_logic;",
                 fmt_vectorType      = "{0: <30} : std_logic_vector({1} downto 0);",
                 fmt_arrayType       = "{0: <30} : {1}{2}_t_ARRAY;",
                 fmt_description     = "  -- {0}",
                 fmt_defaultComplex  = "{0} => Default{1}{2}",
                 fmt_defaultArray    = "{0} => Default{1}{2}_t_ARRAY",
                 fmt_defaultType     = "{0} => X\"{1}\"",
                 fmt_defaultEmpty    = "{0} => (others => '0')",
                 fmt_alias           = "subtype {1}_t is {0}{2}_t;",
                 defaultEmpty        = "(others d=> '0')"):
        self.baseName              = baseName
        self.fmt_complexType       = fmt_complexType    
        self.fmt_bitType           = fmt_bitType        
        self.fmt_vectorType        = fmt_vectorType
        self.fmt_arrayType         = fmt_arrayType
        self.fmt_description       = fmt_description
        self.fmt_defaultComplex    = fmt_defaultComplex
        self.fmt_defaultArray      = fmt_defaultArray
        self.fmt_defaultType       = fmt_defaultType
        self.fmt_defaultEmpty      = fmt_defaultEmpty
        self.fmt_alias             = fmt_alias
        self.defaultEmpty          = defaultEmpty
        
vhdlPackage = PackageStruct()
svPackage = PackageStruct(baseName            = "Params",
                          fmt_complexType     = "t{1}{2: <30}   {0};",
                          fmt_bitType         = "logic                                   {0};",
                          fmt_vectorType      = "logic [{1: >3}:0]                           {0};",
                          fmt_arrayType       = "t{1}{2: <30}   {0} [{3}:{4}];",
                          fmt_description     = "  // {0}",
                          fmt_defaultComplex  = "Default{1}{2}",
                          fmt_defaultArray    = "'{{{3}{{Default{1}{2}}}}}",
                          fmt_defaultType     = "'h{1}",
                          fmt_defaultEmpty    = "0",
                          fmt_alias           = "typedef t{1} t{0}{2};",
                          defaultEmpty        = "0")


#################################################################################
# Package structs
#################################################################################
class PackageType():
    def __init__(self):
        self.name = ""
        self.entries = []
        self.defaults = []
        self.aliases = []
        
#################################################################################
# HDLBuilder
#################################################################################
class HDLBuilder:
    def __init__(self,filename):
        #load the address table
        AddressTable.SetArrayCharacters('[',']')
        self.table = AddressTable(filename)

        #fill tree
        self.ctrl                  = {}
        self.status                = {}
        self.decoderWriteMap       = {}
        self.decoderReadMap        = {}
        self.continuousAssignments = {}
        self.defaultAssignments    = {}
        self.pulseAssignments      = {}
        self.regMaxAddress         = 0
        self.tree = self.BuildTree(self.table.GetBaseItem())


        
    #############################################################################
    #Primary Tree
    #############################################################################
    def __ProcessWriteEntry(self,node):
        address = node.GetAddress()

        #check if this address is in the dictionary of decoder entries
        if address not in self.decoderWriteMap:
            self.decoderWriteMap[address] = {}

            
        #this is also a read reg, so we need to add in the readback
        if node.GetMode()   & int(ModeMask.READ):
            #this is a rw register, so write to a reg assignement
            if address not in self.continuousAssignments:
                self.continuousAssignments[address] = {}
            self.continuousAssignments[address][node.GetFullName()] = node
            #add an entry that starts with a "." to notify this is a rw reg
            self.decoderWriteMap[address]["."+node.GetFullName()] = node
            #update the max reg addr
            if address > self.regMaxAddress:
                self.regMaxAddress = address
        else:
            self.decoderWriteMap[address][node.GetFullName()] = node

        #add default
        if "default" in node.GetUser().keys():
            self.defaultAssignments[node.GetTypeName()] = node.GetUser()["default"]
        else:
            self.defaultAssignments[node.GetTypeName()] = 0

    def __ProcessActionEntry(self,node):
        address = node.GetAddress()

        #check if this address is in the dictionary of decoder entries
        if address not in self.decoderWriteMap:
            self.decoderWriteMap[address] = {}
        self.decoderWriteMap[address][node.GetFullName()] = node

        #Setup default and pulse signals
        self.pulseAssignments[node.GetFullName()] = node #node.userData["default"]
        if "default" in node.GetUser().keys():
            self.defaultAssignments[node.GetParent().GetTypeName()] = node.userData["default"]
        else:
            self.defaultAssignments[node.GetParent().GetTypeName()] = 0

    def __ProcessReadEntry(self,node):
        address = node.GetAddress()

        #check if this address is in the dictionary of decoder entries
        if address not in self.decoderReadMap:
            self.decoderReadMap[address] = {}
            
        #this is also a write reg, so we need to add in the readback
        if node.GetMode()   & int(ModeMask.WRITE):
            #this is a rw register, so write to a reg assignement
            #put an unallowed char "," at the beginning to note we need to make a reg entry
            self.decoderReadMap[address][","+node.GetFullName()] = node
        else:
            self.decoderReadMap[address][node.GetFullName()] = node

            

            
    def BuildTree(self,node):

        children = node.GetChildren()
        if len(children) == 0:
            #simple reg

            #Process this entry as a write entry
            if node.GetMode() & int(ModeMask.WRITE):
                self.__ProcessWriteEntry(node)

            #Process this entry as an action entry
            if node.GetMode() & int(ModeMask.ACTION):
                self.__ProcessActionEntry(node)

            #Process this entry if it a read entry
            if node.GetMode()   & int(ModeMask.READ):
                self.__ProcessReadEntry(node)
        else:
            #creat this type
            self.ctrl[node.GetTypeName()]   = {}
            self.status[node.GetTypeName()] = {}
            if "alias" in node.GetUser():
                #make an alias for the parent
                self.ctrl[node.GetTypeName()][node.GetUser()["alias"]] = None
                self.status[node.GetTypeName()][node.GetUser()["alias"]] = None

            for child in children:                
                if child.IsArrayElement():
                    if child.GetName() not in self.ctrl[node.GetTypeName()]:
                        self.ctrl[node.GetTypeName()][child.GetName()] = {}
                    self.ctrl[node.GetTypeName()][child.GetName()][child.GetArrayIndex()] = child
                    if child.GetName() not in self.status[node.GetTypeName()]:
                        self.status[node.GetTypeName()][child.GetName()] = {}
                    self.status[node.GetTypeName()][child.GetName()][child.GetArrayIndex()] = child
                else:
                    if child.GetMode() > 0:
                        self.ctrl[node.GetTypeName()][child.GetName()]   = child
                        self.status[node.GetTypeName()][child.GetName()] = child
                    else:
                        self.ctrl[node.GetTypeName()][child.GetName()]   = child.GetTypeName()
                        self.status[node.GetTypeName()][child.GetName()] = child.GetTypeName()                    
                self.BuildTree(child)
                

                    
    #############################################################################
    #Packages
    #############################################################################    
    def GeneratePackages(self,
                         name,
                         entries,
                         formatting,
                         buildDefault = False
                         ):
        
        entry = entries[name]
        
        package = PackageType()
        package.name = formatting.baseName+name
        
        
        #go through all the entries and build any needed entries
        for subEntry,value in entry.items():
            if isinstance(value,dict):
                #this is an array entry, build an entry for one of the array values
                self.GeneratePackages(value[next(iter(value))].GetTypeName(),
                                      entries,
                                      formatting,
                                      buildDefault)
            elif isinstance(value,Item):
                #this is a register, so no type needs to be built
                pass
            elif value is None:
                #this is an alias
                pass
            else:
                #this is a complex data structure
                self.GeneratePackages(value,
                                      entries,
                                      formatting,
                                      buildDefault)


        #build the entries for the package
        for subEntry,value in entry.items():
            if isinstance(value,dict):
                #build an array type

                #compute the array size
                maxIndex = 0
                minIndex = 9999999
                indexCount = 0
                for index in value.keys():
                    if int(index) > maxIndex:
                        maxIndex = int(index)
                    if int(index) < minIndex:
                        minIndex = int(index)
                    indexCount=indexCount+1
                if indexCount != (1 + maxIndex - minIndex):
                    raise ValueError('Index range %d to %d didn\'t match the count %d' % (minIndex,maxIndex,indexCount))
                    
                #built the line ofr this
                structLine = formatting.fmt_arrayType.format(subEntry,
                                                             formatting.baseName,
                                                             value[next(iter(value))].GetTypeName(),
                                                             minIndex,maxIndex
                                                             )
                #build the array type
                #TODO for VHDL
                
                
                #add the default value
                if buildDefault:
                    defaultLine = formatting.fmt_defaultArray.format(subEntry,
                                                                     formatting.baseName,
                                                                     value[next(iter(value))].GetTypeName(),
                                                                     indexCount) 
                    
                
            elif isinstance(value,Item):
                #this is a real reg entry

                #decide if this is a 1 bit signal or a vector
                bitCount = 1 + value.GetMaskBitHigh() - value.GetMaskBitLow()                
                if bitCount == 1:
                    #this is a 1 bit entry
                    structLine = formatting.fmt_bitType.format(subEntry)
                else:
                    #this is a multibit vector entry
                    structLine = formatting.fmt_vectorType.format(subEntry,
                                                                  bitCount-1)

                userData = value.GetUser()
                #add a description if it is set
                if "description" in userData.keys():
                    structLine += formatting.fmt_description.format(userData["description"])

                #add the default value
                if buildDefault:
                    if "default" in userData.keys():
                        defaultLine = formatting.fmt_defaultType.format(subEntry,
                                                                        hex(int(userData["default"],0))[2:])
                    else:
                        defaultLine = formatting.fmt_defaultEmpty.format(subEntry)

            elif value is None:
                #this is an alias
                package.aliases.append(formatting.fmt_alias.format(formatting.baseName,
                                                                   package.name,
                                                                   subEntry))
                structLine = None
            else:                
                #this is a complex type
                structLine = formatting.fmt_complexType.format(subEntry,
                                                               formatting.baseName,
                                                               value)
                if buildDefault:
                    defaultLine = formatting.fmt_defaultComplex.format(subEntry,
                                                                       formatting.baseName,
                                                                       value) 

            if structLine is not None:
                package.entries.append(structLine)
                if buildDefault:
                    package.defaults.append(defaultLine)
        self.packageEntries.append(package)        
        
    def WritePackages(self,
                      pkg_name,
                      ctrlFormatting,
                      statusFormatting,
                      template_filename,
                      output_filename):        
        #clear anything old
        self.packageEntries = []
        
        #Generate the packages
        self.GeneratePackages('',
                              self.ctrl,
                              ctrlFormatting,
                              buildDefault=True
                              )
        self.GeneratePackages('',
                              self.status,
                              statusFormatting)
        
        #Using the template to generate the decoder package
        with open(template_filename) as template_input_file:
            pkgOutput = template_input_file.read()
            pkgOutput = Template(pkgOutput)
            template_input_file.close()
        
        substitute_mapping = {
            "packageName" : pkg_name,
            "outStructures" : self.packageEntries
        }
        
        pkgOutput = pkgOutput.render(substitute_mapping)
        # output to file
        with open(output_filename, 'w') as outFile:
            outFile.write(pkgOutput)
            outFile.close()


    def WriteDecoder(self,
                     pkg_name,
                     ctrlFormatting,
                     template_filename,
                     output_filename):
        #build action reset list
        actions = {}
        for name,entry in self.pulseAssignments.items():
            if "default" in entry.GetUser():
                actions[name] = entry.GetUser()["default"]
            else:
                actions[name] = ctrlFormatting.defaultEmpty                
                   
        #build write list (action + write)
        writeDict = {}
        for addr,entries in self.decoderWriteMap.items():
            if addr not in writeDict.keys():
                writeDict[addr] = {}
            for name,entry in entries.items():
                writeDict[addr][name] = entry

        writes = {}
        for addr,entries in writeDict.items():
            if addr not in writes.keys():
                writes[addr] = {}            
            for name,entry in entries.items():
                if name not in writes[addr].keys():
                    writes[addr][name] = {}
                    writes[addr][name]["bitStart"] = (entry.GetOffset() +
                                                      bin(entry.GetMask())[2:].count('1') -
                                                      1)
                writes[addr][name]["bitEnd"]   = entry.GetOffset()
                userData = entry.GetUser()                
                if "description" in userData.keys(): 
                    writes[addr][name]["description"] = userData["description"]
                else:
                    writes[addr][name]["description"] = ""
            
        #build read list
        reads = {}
        for addr,entries in self.decoderReadMap.items():
            if addr not in reads.keys():
                reads[addr] = {}            
            for name,entry in entries.items():
                if name not in reads[addr].keys():
                    reads[addr][name] = {}

                reads[addr][name]["bitStart"] = (entry.GetOffset() +
                                                 bin(entry.GetMask())[2:].count('1') -
                                                 1)
                reads[addr][name]["bitEnd"]   = entry.GetOffset()
                userData = entry.GetUser()
                if "description" in userData.keys(): 
                    reads[addr][name]["description"] = userData["description"]
                else:
                    reads[addr][name]["description"] = ""


        with open(template_filename) as template_input_file:
            pkgOutput = template_input_file.read()
            pkgOutput = Template(pkgOutput)
            template_input_file.close()
        

        substitute_mapping = {
            "packageName"           : pkg_name,
            "continuousAssignments" : dict(sorted(self.continuousAssignments.items())),
            "actions"               : dict(sorted(actions.items())),
            "writeList"             : dict(sorted(writes.items())),
            "readList"              : dict(sorted(reads.items())),
            "maxAddress"            : self.regMaxAddress
        }
    
        pkgOutput = pkgOutput.render(substitute_mapping)
        # output to file
        with open(output_filename, 'w') as outFile:
            outFile.write(pkgOutput)
            outFile.close()
        
            
            
import argparse
if __name__ == "__main__":    
    parser = argparse.ArgumentParser(prog='parse')
    parser.add_argument('addrtable_filename')
    parser.add_argument('template_type')
    parser.add_argument('output_path')
    parser.add_argument('-n', '--output_name',   default="")
    parser.add_argument('-p', '--package_name',  default="pkg_regs")
    parser.add_argument('-c', '--ctrl_pkg_base', default="Params")
    parser.add_argument('-s', '--status_pkg_base', default="Status")
    
    args = parser.parse_args()    
    hdl=HDLBuilder(os.path.realpath(os.path.join(os.getcwd(),args.addrtable_filename)))

    #select formatter
    if args.template_type == "vhd":
        ctrlPkg = copy(vhdlPackage)        
        statusPkg = copy(vhdlPackage)        
    elif args.template_type == "sv":
        #create the verilog version
        ctrlPkg = copy(svPackage)        
        statusPkg = copy(svPackage)        
    ctrlPkg.baseName   = args.ctrl_pkg_base
    statusPkg.baseName = args.status_pkg_base

        
    hdl.WritePackages(args.package_name,
                      ctrlPkg,
                      statusPkg,
                      os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                   "templates/",
                                   args.template_type,
                                   "package."+args.template_type+"_part"),
                      args.output_path + "/" + args.output_name + "pkg."+args.template_type)

    hdl.WriteDecoder(args.package_name,
                     ctrlPkg,
                     os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                  "templates/",
                                  args.template_type,
                                  "decoder."+args.template_type+"_part"),
                     args.output_path + "/" + args.output_name + "decoder."+args.template_type)
