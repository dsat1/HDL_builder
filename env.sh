SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
pushd $SCRIPT_DIR   > /dev/null
pushd external/dsat > /dev/null

source env.sh

popd                > /dev/null
popd                > /dev/null
